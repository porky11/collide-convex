#![deny(missing_docs)]

/*!
This crate contains a convex collider, which implements the collide trait from the `collide` crate.
**/

use collide::{Collider, CollisionInfo};
use num_traits::{real::Real, Zero};
use vector_space::{InnerSpace, VectorSpace};

#[derive(Clone)]
/// The convex collider defined as a convex hull around multiple spheres having the same radius.
pub struct Convex<V: VectorSpace> {
    /// The radius of the spheres.
    pub rad: V::Scalar,
    /// The positions of the spheres.
    pub points: Vec<V>,
}

impl<V: VectorSpace> Convex<V> {
    /// Creates a new convex collider.
    pub fn new(rad: V::Scalar, points: Vec<V>) -> Self {
        Self { rad, points }
    }

    /// Creates a new convex collider representing a capsule.
    pub fn capsule(rad: V::Scalar, start: V, end: V) -> Self {
        Self {
            rad,
            points: vec![start, end],
        }
    }

    /// Creates a new convex collider representing a point.
    pub fn point(pos: V) -> Self {
        Self {
            rad: V::Scalar::zero(),
            points: vec![pos],
        }
    }

    /// Creates a new convex collider representing a line.
    pub fn line(start: V, end: V) -> Self {
        Self {
            rad: V::Scalar::zero(),
            points: vec![start, end],
        }
    }

    /// Creates a new convex collider representing a sphere.
    pub fn sphere(pos: V, rad: V::Scalar) -> Self {
        Self {
            rad,
            points: vec![pos],
        }
    }
}

fn origin_offset<V: InnerSpace>(segment: &[V]) -> V {
    let mut points = segment.iter();
    let Some(&(mut offset)) = points.next() else {
        return V::zero();
    };
    for &segment_point in points {
        offset = offset + (-offset).project(segment_point - offset);
    }
    offset
}

fn origin_side<V: InnerSpace>(point: V, segment: &[V]) -> bool {
    origin_offset(segment).scalar(point - segment[0]) <= V::Scalar::zero()
}

struct Simplex<V> {
    points: Vec<V>,
}

impl<V: InnerSpace> Simplex<V> {
    fn new(initial: V) -> Self {
        Self {
            points: vec![initial],
        }
    }

    fn add(&mut self, new_point: V) -> Option<V> {
        if self.points.contains(&new_point) {
            return None;
        }

        self.points.push(new_point);

        let count = self.points.len();

        let side_count = count - 1;
        let mut side = vec![V::zero(); side_count];

        let removed: Vec<_> = (0..count)
            .map(|i| {
                let point = self.points[i];
                for (s, side_point) in side.iter_mut().enumerate() {
                    let i = if s < i { s } else { s + 1 };
                    *side_point = self.points[i];
                }
                !origin_side(point, &side)
            })
            .collect();

        for (i, removed) in removed.into_iter().enumerate().rev() {
            if removed {
                self.points.remove(i);
            }
        }

        Some(-origin_offset(&self.points))
    }
}

impl<V: InnerSpace> Convex<V> {
    fn support(&self, other: &Self, dir: V) -> V {
        let mut found: Option<V> = None;
        for &point in &self.points {
            for &other_point in &other.points {
                let diff = other_point - point;
                if found.map_or(false, |found| diff.scalar(dir) <= found.scalar(dir)) {
                    continue;
                }
                found = Some(diff)
            }
        }

        found.unwrap_or_else(|| V::zero())
    }

    fn offset(&self, other: &Self) -> V {
        //std::thread::sleep(std::time::Duration::from_millis(500));

        // Find an initial simplex vertex on the object, choose initial search direction
        let isd = other.points[0] - self.points[0];
        if isd.is_zero() {
            return V::zero();
        }
        let initial = self.support(other, isd);
        let mut simplex = Simplex::new(initial);

        let mut dir = -initial;

        // Find the farthest point on the object in the search direction and add it to the simplex
        while let Some(new_dir) = simplex.add(self.support(other, dir)) {
            dir = new_dir;
        }

        // TODO: handle case, where origin is inside simplex (EPA)

        origin_offset(&simplex.points)
    }
}

impl<V: InnerSpace> Collider for Convex<V> {
    type Vector = V;

    fn collision_info(&self, other: &Self) -> Option<CollisionInfo<Self::Vector>> {
        let offset = self.offset(other);
        let mag2 = offset.magnitude2();
        let rad = self.rad + other.rad;
        if mag2 > rad * rad {
            return None;
        }

        let mag = mag2.sqrt();
        let dir = offset / mag;
        Some(CollisionInfo {
            self_contact: V::zero(),
            other_contact: V::zero(),
            vector: dir * (mag - rad),
        })
    }
}
