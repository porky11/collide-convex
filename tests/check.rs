/**
This checks collisions between different convexs for narrow cases.

The test names are constructed like this:
`<shape1>_<shape2>_match<dimension>[_not]`

The shapes (`shape1`, `shape2`) are one of these shapes:
* `point`: Just a point represented as a convex
* `sphere`: Just a sphere represented as a convex
* `cylindrical`: A real convex; collision takes place at the cylindrical part
* `spherical`: A real convex; collision takes place at the spherical part

Each combination is tested once with different options.

If both shapes are the same, the test name is constructed like this:
`<shape>_match<dimension>[_not]`

The tests are only executed for the most common dimensions 2 and 3 while using `f32` as element type.

Each test also has a variant suffixed with "_not", which tests a case, where the shapes should just not collide.
**/
use collide::Collider;
use collide_convex::Convex;
use simple_vectors::Vector;
use vector_space::InnerSpace;

fn check<const N: usize>(a: Convex<Vector<f32, N>>, b: Convex<Vector<f32, N>>) {
    assert!(a.check_collision(&b))
}

fn check_not<const N: usize>(a: Convex<Vector<f32, N>>, b: Convex<Vector<f32, N>>) {
    assert!(!a.check_collision(&b))
}

#[test]
fn point_match2() {
    check(
        Convex::point(Vector::new([0.0; 2])),
        Convex::point(Vector::new([0.0; 2])),
    );
}

#[test]
fn point_match2_not() {
    check_not(
        Convex::point(Vector::new([0.0; 2])),
        Convex::point(Vector::new([0.01; 2])),
    );
}

#[test]
fn point_match3() {
    check(
        Convex::point(Vector::new([1.0, 2.0, 3.0])),
        Convex::point(Vector::new([1.0, 2.0, 3.0])),
    );
}

#[test]
fn point_match3_not() {
    check_not(
        Convex::point(Vector::new([1.0, 2.0, 3.0])),
        Convex::point(Vector::new([1.01, 2.0, 3.0])),
    );
}

#[test]
fn point_sphere_match2() {
    check(
        Convex::point(Vector::new([1.0, 0.0])),
        Convex::sphere(Vector::new([-1.0, 0.0]), 2.0),
    );
}

#[test]
fn point_sphere_match2_not() {
    check_not(
        Convex::point(Vector::new([1.0, 0.0])),
        Convex::sphere(Vector::new([-1.01, 0.0]), 2.0),
    );
}

#[test]
fn point_sphere_match3() {
    check(
        Convex::point(Vector::new([0.577; 3])),
        Convex::sphere(Vector::new([0.0; 3]), 1.0),
    );
}

#[test]
fn point_sphere_match3_not() {
    check_not(
        Convex::point(Vector::new([0.578; 3])),
        Convex::sphere(Vector::new([0.0; 3]), 1.0),
    );
}

#[test]
fn point_cylindrical_match2() {
    check(
        Convex::point(Vector::new([1.414, -1.414])),
        Convex::capsule(2.0, Vector::new([-1.0; 2]), Vector::new([1.0; 2])),
    );
}

#[test]
fn point_cylindrical_match2_not() {
    check_not(
        Convex::point(Vector::new([1.415, -1.415])),
        Convex::capsule(2.0, Vector::new([-1.0; 2]), Vector::new([1.0; 2])),
    );
}

#[test]
fn point_cylindrical_match3() {
    check(
        Convex::point(Vector::new([0.0, 0.0, 0.0])),
        Convex::capsule(
            5.0,
            Vector::new([3.535, -3.535, -2.0]),
            Vector::new([3.535, -3.535, 2.0]),
        ),
    );
}

#[test]
fn point_cylindrical_match3_not() {
    check_not(
        Convex::point(Vector::new([0.0, 0.01, 0.0])),
        Convex::capsule(
            5.0,
            Vector::new([3.535, -3.535, -2.0]),
            Vector::new([3.535, -3.535, 2.0]),
        ),
    );
}

#[test]
fn point_spherical_match2() {
    check(
        Convex::point(Vector::new([3.0, 0.0])),
        Convex::capsule(1.0, Vector::new([-2.0, 0.0]), Vector::new([2.0, 0.0])),
    );
}

#[test]
fn point_spherical_match2_not() {
    check_not(
        Convex::point(Vector::new([3.0, 0.01])),
        Convex::capsule(1.0, Vector::new([-2.0, 0.0]), Vector::new([2.0, 0.0])),
    );
}

#[test]
fn point_spherical_match3() {
    check(
        Convex::point(Vector::new([2.0; 3]) + Vector::new([1.414, -1.414, 0.0])),
        Convex::capsule(2.0, Vector::new([-2.0; 3]), Vector::new([2.0; 3])),
    );
}

#[test]
fn point_spherical_match3_not() {
    check_not(
        Convex::point(Vector::new([2.0; 3]) + Vector::new([1.415, -1.415, 0.0])),
        Convex::capsule(2.0, Vector::new([-2.0; 3]), Vector::new([2.0; 3])),
    );
}

#[test]
fn sphere_match2() {
    check(
        Convex::sphere(Vector::new([1.0, 0.0]), 100.0),
        Convex::sphere(Vector::new([-100.0, 0.0]), 1.0),
    );
}

#[test]
fn sphere_match2_not() {
    check_not(
        Convex::sphere(Vector::new([1.01, 0.0]), 100.0),
        Convex::sphere(Vector::new([-100.0, 0.0]), 1.0),
    );
}

#[test]
fn sphere_match3() {
    check(
        Convex::sphere(Vector::new([0.0; 3]), 5.0),
        Convex::sphere(Vector::new([5.773; 3]), 5.0),
    );
}

#[test]
fn sphere_match3_not() {
    check_not(
        Convex::sphere(Vector::new([0.0; 3]), 4.99),
        Convex::sphere(Vector::new([5.773; 3]), 5.0),
    );
}

#[test]
fn sphere_cylindrical_match2() {
    check(
        Convex::sphere(Vector::new([1.414, -1.414]), 1.0),
        Convex::capsule(1.0, Vector::new([-1.0; 2]), Vector::new([1.0; 2])),
    );
}

#[test]
fn sphere_cylindrical_match2_not() {
    check_not(
        Convex::sphere(Vector::new([1.414, -1.414]), 1.0),
        Convex::capsule(0.99, Vector::new([-1.0; 2]), Vector::new([1.0; 2])),
    );
}

#[test]
fn sphere_cylindrical_match3() {
    check(
        Convex::sphere(Vector::new([0.0, 0.0, 4.0]), 2.0),
        Convex::capsule(
            2.0,
            Vector::new([-1.0, -1.0, 0.0]),
            Vector::new([1.0, 1.0, 0.0]),
        ),
    );
}

#[test]
fn sphere_cylindrical_match3_not() {
    check_not(
        Convex::sphere(Vector::new([0.0, 0.0, 4.01]), 2.0),
        Convex::capsule(
            2.0,
            Vector::new([-1.0, -1.0, 0.0]),
            Vector::new([1.0, 1.0, 0.0]),
        ),
    );
}

#[test]
fn sphere_spherical_match2() {
    check(
        Convex::sphere(Vector::new([2.0, 5.0]), 2.0),
        Convex::capsule(1.0, Vector::new([-4.0; 2]), Vector::new([2.0; 2])),
    );
}

#[test]
fn sphere_spherical_match2_not() {
    check_not(
        Convex::sphere(Vector::new([2.0, 5.01]), 2.0),
        Convex::capsule(1.0, Vector::new([-4.0; 2]), Vector::new([2.0; 2])),
    );
}

#[test]
fn sphere_spherical_match3() {
    check(
        Convex::sphere(Vector::new([5.0, 0.0, 0.0]) + Vector::new([-4.041; 3]), 2.0),
        Convex::capsule(
            5.0,
            Vector::new([5.0, 0.0, 0.0]),
            Vector::new([6.0, 0.0, 0.0]),
        ),
    );
}

#[test]
fn sphere_spherical_match3_not() {
    check_not(
        Convex::sphere(Vector::new([5.0, 0.0, 0.0]) + Vector::new([-4.042; 3]), 2.0),
        Convex::capsule(
            5.0,
            Vector::new([5.0, 0.0, 0.0]),
            Vector::new([6.0, 0.0, 0.0]),
        ),
    );
}

#[test]
fn cylindrical_match2() {
    check(
        Convex::capsule(2.0, Vector::new([-1.0, 1.0]), Vector::new([-1.0, -1.0])),
        Convex::capsule(1.0, Vector::new([2.0, 0.0]), Vector::new([2.0, -2.0])),
    );
}

#[test]
fn cylindrical_match2_not() {
    check_not(
        Convex::capsule(2.0, Vector::new([-1.0, 1.0]), Vector::new([-1.01, -1.0])),
        Convex::capsule(1.0, Vector::new([2.0, 0.0]), Vector::new([2.0, -2.0])),
    );
}

#[test]
fn cylindrical_match3() {
    check(
        Convex::capsule(
            1.0,
            Vector::new([-1.0, -1.0, 0.0]),
            Vector::new([1.0, 1.0, 0.0]),
        ),
        Convex::capsule(
            1.0,
            Vector::new([-1.414, 1.414, -5.0]),
            Vector::new([-1.414, 1.414, 5.0]),
        ),
    );
}

#[test]
fn cylindrical_match3_not() {
    check_not(
        Convex::capsule(
            1.0,
            Vector::new([-1.0, -1.0, 0.0]),
            Vector::new([1.0, 1.0, 0.0]),
        ),
        Convex::capsule(
            1.0,
            Vector::new([-1.415, 1.415, -5.0]),
            Vector::new([-1.415, 1.415, 5.0]),
        ),
    );
}

#[test]
fn cylindrical_spherical_match2() {
    check(
        Convex::capsule(1.0, Vector::new([0.0, -1.0]), Vector::new([0.0, 1.0])),
        Convex::capsule(2.0, Vector::new([-5.0, 0.0]), Vector::new([-3.0, 0.0])),
    );
}

#[test]
fn cylindrical_spherical_match2_not() {
    check_not(
        Convex::capsule(1.0, Vector::new([0.0, -1.0]), Vector::new([0.0, 1.0])),
        Convex::capsule(2.0, Vector::new([-5.0, 0.0]), Vector::new([-3.01, 0.0])),
    );
}

#[test]
fn cylindrical_spherical_match3() {
    let dir = Vector::new([1.0, 1.0, -1.0]).normalize();
    let start = Vector::new([-2.0; 3]) + dir * 2.12;
    check(
        Convex::capsule(1.0, Vector::new([-5.0; 3]), Vector::new([5.0; 3])),
        Convex::capsule(1.0, start, start + dir),
    );
}

#[test]
fn cylindrical_spherical_match3_not() {
    let dir = Vector::new([1.0, 1.0, -1.0]).normalize();
    let start = Vector::new([-2.0; 3]) + dir * 2.13;
    check_not(
        Convex::capsule(1.0, Vector::new([-5.0; 3]), Vector::new([5.0; 3])),
        Convex::capsule(1.0, start, start),
    );
}

#[test]
fn spherical_match2() {
    check(
        Convex::capsule(2.0, Vector::new([2.0, 5.0]), Vector::new([4.0, 5.0])),
        Convex::capsule(1.0, Vector::new([-4.0; 2]), Vector::new([2.0; 2])),
    );
}

#[test]
fn spherical_match2_not() {
    check_not(
        Convex::capsule(2.0, Vector::new([2.0, 5.01]), Vector::new([4.0, 5.01])),
        Convex::capsule(1.0, Vector::new([-4.0; 2]), Vector::new([2.0; 2])),
    );
}

#[test]
fn spherical_match3() {
    let start = Vector::new([5.0, 0.0, 0.0]) + Vector::new([4.041; 3]);
    check(
        Convex::capsule(2.0, start, start + Vector::new([0.0, 5.0, 0.0])),
        Convex::capsule(
            5.0,
            Vector::new([0.0, 0.0, 0.0]),
            Vector::new([5.0, 0.0, 0.0]),
        ),
    );
}

#[test]
fn spherical_match3_not() {
    let start = Vector::new([5.0, 0.0, 0.0]) + Vector::new([4.042; 3]);
    check_not(
        Convex::capsule(2.0, start, start + Vector::new([0.0, 5.0, 0.0])),
        Convex::capsule(
            5.0,
            Vector::new([0.0, 0.0, 0.0]),
            Vector::new([5.0, 0.0, 0.0]),
        ),
    );
}
